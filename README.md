# Installation and run with Docker
```bash
cd tech-assesment-car
sudo docker-compose up --build # takes also about 5 min
```
#### go http://0.0.0.0:8000/#/

```
[real-time tracking] - yes
[playback] - yes
[backend responsible for playback] - yes
[use websockets] - yes
[render road realtime] - yes
[navigate history with disconnected backend] - yes
[snap routes along roads] - yes
[delay configurable] - yes
[throtling incoming updates] - yes
[multy browser-tabs support] - yes

[PWA] not easy, took long time, not implemented
[randomized time delay] - no
[support more than one worker] - no
```

![histiry](./screenshot.png)


# Installation and run manually
#### Installation

```
cd tech-assesment-car

pushd backend
   python3 -m venv venv
   source venv/bin/activate
   pip3 install -r requirements.txt
popd

pushd frontend
   yarn install
   yarn build
popd
```

#### Run

```
source backend/venv/bin/activate
python manage.py
```
#### go http://127.0.0.1:5000/#/
