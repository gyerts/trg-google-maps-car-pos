import json

trips = []
live_all_coordinates = []


with open("trips.geo.json", "r") as read_file:
    content = json.load(read_file)

    for feature in content['features']:
        trip = dict()
        trip['id'] = str(len(trips))
        trip['name'] = feature['properties']['name']
        trip['coordinates'] = [{'lng': pair[0], 'lat': pair[1]} for pair in feature['geometry']['coordinates'][0]]
        trips.append(trip)

with open("live.geo.json", "r") as read_file:
    content = json.load(read_file)
    feature = content['features'][0]
    live_all_coordinates = [{'lng': pair[0], 'lat': pair[1]} for pair in feature['geometry']['coordinates'][0]]
