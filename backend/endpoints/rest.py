from flask_restful import Resource
from backend.db import trips


class History(Resource):
    @staticmethod
    def get():
        return {
            'data': {
                'trips': trips,
            },
            'statusCode': 200,
        }


def init_rest(api):
    api.add_resource(History, '/api/history')
