from flask_socketio import join_room, leave_room
from flask import request
from backend.db import trips, live_all_coordinates

socket = None

running_playbacks = []
live_is_running = False
live_current_trip = []
sid_settings = {}

def init_ws(socketio):
    socket = socketio

    @socketio.on('run live globally')
    def run_live_globally():
        global live_is_running
        global live_current_trip

        if live_is_running:
            return

        live_is_running = True
        while True:
            socketio.emit('set coords', [])
            for i, coords in enumerate(live_all_coordinates):
                if socketio:
                    print('live coords', i, coords)
                    live_current_trip = live_all_coordinates[0:i]
                    socketio.sleep(seconds=2)
                    socketio.emit('coords', coords, room='live')
                else:
                    print('no socket')

    @socketio.on('run live')
    def run_live(data):
        global live_current_trip
        join_room('live', data['sid'])
        socketio.emit('set coords', live_current_trip)

    @socketio.on('stop live')
    def stop_live(data):
        leave_room('live', data['sid'])

    @socketio.on('run playback')
    def run_playback(data):
        trip_id = data['id']
        sid = data['sid']

        found_trip = next((trip for trip in trips if trip['id'] == trip_id), None)

        if found_trip:
            running_playbacks.append(sid)

            for i, coords in enumerate(found_trip['coordinates']):
                speed = sid_settings[sid]['speed']

                if sid in running_playbacks:
                    print('speed', 2/speed)
                    socket.sleep(seconds=2/speed)
                    socket.emit('coords', coords, room=sid)

                else:
                    break

            socket.emit('end playback')

    @socketio.on('stop playback')
    def stop_playback(data):
        global running_playbacks

        sid = data['sid']
        running_playbacks = list(filter(lambda _sid: _sid != sid, running_playbacks))

    @socketio.on('set speed')
    def set_speed(data):
        settings = sid_settings[data['sid']]
        settings['speed'] = data['speed'] if data['speed'] != 0 else 0.1

    @socketio.on('join_to_room')
    def set_speed(data):
        join_room(data['sid'], data['sid'])

    @socketio.on('connect')
    def connected():
        sid = request.sid
        socketio.emit('sid', {'sid': sid})

        sid_settings[sid] = {
            'speed': 1
        }

    @socketio.on('disconnect')
    def disconnected():
        print('disconnected')

    return socketio
