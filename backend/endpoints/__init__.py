from .ws import init_ws
from .static import init_static
from .rest import init_rest
