from flask import send_from_directory


def init_static(app):
    @app.route('/')
    def serve_static_index():
        return send_from_directory('./frontend/build/', 'index.html')

    @app.route('/<path:filename>')
    def custom_static(filename):
        return send_from_directory('./frontend/build/', filename)
