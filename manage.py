from flask import Flask
from flask_socketio import SocketIO
from flask_session import Session
from flask_restful import Api
from flask_cors import CORS


from backend.endpoints import init_ws, init_static, init_rest

app = Flask(__name__, static_folder='./frontend/build/static', static_url_path='/static')
app.config['SECRET_KEY'] = 'secret!'

Session(app)
socketio = SocketIO(app, cors_allowed_origins="*", manage_session=False)
api = Api(app)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

init_static(app)
socketio = init_ws(socketio)
init_rest(api)


if __name__ == '__main__':
    socketio.run(app)
