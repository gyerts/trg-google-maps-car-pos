const {
   override,
   fixBabelImports,
   addLessLoader,
   addBabelPlugin,
   babelInclude,
   addDecoratorsLegacy,
   addWebpackAlias,
} = require('customize-cra');


const path = require("path");

module.exports = override(
   fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: true,
   }),
   babelInclude([
      path.resolve('src'),
      path.resolve(process.env.PWD, '../common'),
      path.resolve(process.env.PWD, '../../shared'),
   ]),
   addWebpackAlias({
      "@utils": path.resolve(__dirname, "./src/utils"),
      "@shared": path.resolve(__dirname, "./src/shared"),
      "@common": path.resolve(__dirname, "./src/common"),
      "@resources": path.resolve(__dirname, "./src/resources"),
      "@components": path.resolve(__dirname, "./src/components"),
      "@models": path.resolve(__dirname, "./src/models"),
      "@app": path.resolve(__dirname, "./src/apps/app"),
      "@auth": path.resolve(__dirname, "./src/apps/auth"),
      "@ps": path.resolve(__dirname, "./src/ps"),
      "@env": path.resolve(__dirname, "./src/env"),
      "@api": path.resolve(__dirname, "./src/api"),
   }),
   addLessLoader({
      javascriptEnabled: true,
      modifyVars: {
         '@primary-color': '#43B0F1',
         '@link-color': '#43B0F1',
         '@success-color': '#52C41A',
         '@warning-color': '#FFBF00',
         '@error-color': '#F4333C',
         '@heading-color': '#162F3D',
         '@text-color': '#162F3D',
      },
   }),
   addDecoratorsLegacy(),
   addBabelPlugin(['module-resolver', {
      root: '../packages',
      alias: {
         common: '../common',
         shared: '../../shared',
      }
   }]),
);
