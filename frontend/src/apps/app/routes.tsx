import {loadable} from "@components/RoutesLoadable";

const prefix = '';
export const appRoutes = {
   main: { url: `${prefix}/` },
};

export const appLoadableRouts = {};

appLoadableRouts[appRoutes.main.url] = {
   component: loadable(() => import('./pages/MainPage')),
};
