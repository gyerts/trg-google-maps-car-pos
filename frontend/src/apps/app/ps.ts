import {configure} from "mobx";
import {WebsocketStore} from "@app/store/websocketStore";
import {AppStore} from "@app/store/appStore";

configure({
   enforceActions: 'observed'
});

export class Store {
   ws = new WebsocketStore();
   history = new AppStore();
}
