import {getHistoryRequest} from "@app/api/read.history";

export const appRequests = {
   history: {
      read: getHistoryRequest,
   },
};
