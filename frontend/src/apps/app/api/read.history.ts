import {fetchWrapper} from "@utils/http/fetchWrapper";
import {l} from "@utils/logger";
import {AUTH_HEADER, BASE_URL, CONTENT_TYPE_HEADER} from "@api/config";

import {ITrip, tripSchema} from "@models/Trip";

import {
   defaultRequiredResponseSchemaFields,
   defaultResponseSchemaFields,
   IDefaultResponseSchema
} from "@api/default";

type IResponseData = {
   trips: ITrip[]
};

type IResponse = IDefaultResponseSchema<IResponseData>;

const responseSchema = {
   type: 'object',
   properties: {
      data: {
         type: 'object',
         properties: {
            trips: {
               type: 'array',
               items: {
                  $ref: tripSchema.id,
               }
            },
         },
         required: [
            'trips',
         ]
      },
      ...defaultResponseSchemaFields,
   },
   required: defaultRequiredResponseSchemaFields,
};

export const getHistoryRequest = async (): Promise<IResponseData> => {
   let response: IResponse = await fetchWrapper<IResponse>(
      BASE_URL(`/history`),
      {
         method: 'GET',
         headers: {
            ...CONTENT_TYPE_HEADER(),
            ...AUTH_HEADER(),
         },
      },
      responseSchema,
   );

   // =======================================
   // update local storage here
   // =======================================

   l.log(response);
   if (!response.data || !response.data.trips) {
      throw response.errors;
   }

   return response.data;
};
