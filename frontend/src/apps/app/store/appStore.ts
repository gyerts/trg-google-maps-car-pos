import {action, observable} from "mobx";
import {ITrip} from "@models/Trip";
import {api} from "@api";
import {ps} from "@ps";

const DEFAULT_COORDS = {lat: 35.095192, lng: 33.203430};
const DEFAULT_ZOOM = 14;
export type ICoords = {lat: number, lng: number};

export class AppStore {
   @observable initialized: boolean = false;
   @observable trips: ITrip[] = [];
   @observable coords: ICoords[] = [];
   @observable lastPoint: ICoords = DEFAULT_COORDS;
   @observable playbackMode: boolean = false;
   @observable liveMode: boolean = true;
   @observable selectedTripId: string|undefined = undefined;
   @observable zoom: number = 0;
   @observable speed: number = 1;
   @observable updates: number = 1;
   coordsBuffer: ICoords[] = [];

   constructor () {
      this.initHistory();
   }

   @action('Run live') runLive = () => {
      this.liveMode = true;
      this.zoom = DEFAULT_ZOOM;
      this.coords = [];
      this.setSelectedTripId();
      ps.app.ws.runLive();
   };

   @action('Stop live') stopLive = () => {
      this.liveMode = false;
      this.setZoom();
      ps.app.ws.stopLive();
   };

   @action('Set speed') setSpeed = (speed: number) => {
      this.speed = speed;
      ps.app.ws.setSpeed(speed);
   };

   @action('Set updates') setUpdates = (updates: number) => {
      this.updates = updates;
      ps.app.ws.setUpdates(updates);
   };

   @action('Run playback') runPlayback = () => {
      const foundTrip = this.trips.find(t => t.id === this.selectedTripId);
      if (this.selectedTripId && foundTrip) {
         this.playbackMode = true;
         this.zoom = DEFAULT_ZOOM;
         this.coords = [];
         this.lastPoint = foundTrip.coordinates[0];
         ps.app.ws.runPlayback(this.selectedTripId);
      } else {
         alert('Select trip first');
      }
   };

   @action('Stop playback') stopPlayback = () => {
      this.playbackMode = false;
      this.setSelectedTripId(this.selectedTripId);
      this.setZoom();
      this.applyBufferedCoords();
      if (this.selectedTripId) {
         ps.app.ws.stopPlayback(this.selectedTripId);
      } else {
         alert('Impossible! trip cannot be not selected!');
      }
   };

   @action('Set selected trip id') setSelectedTripId = (selectedTripId?: string) => {
      this.selectedTripId = selectedTripId;
      if (selectedTripId) {
         const foundTrip = this.trips.find(t => t.id === selectedTripId);
         if (foundTrip) {
            this.liveMode && this.stopLive();
            this.playbackMode && this.stopPlayback();
            this.coords = foundTrip.coordinates;
            this.lastPoint = foundTrip.coordinates[foundTrip.coordinates.length - 1];
         } else {
            alert('Impossible! Cannot find trip!');
         }
      } else {
         this.coords = [];
      }
   };

   @action('Apply buffered coords') applyBufferedCoords = () => {
      this.coords = [...this.coords, ...this.coordsBuffer];
      this.coordsBuffer = [];
   };

   @action('Add coords') addCoords = (coords: ICoords) => {
      if (this.playbackMode || this.liveMode) {
         this.coordsBuffer.push(coords);

         console.log(`[Add coords] per ${this.updates}: ${this.coordsBuffer.length}, total: ${this.coords.length}`);

         if (this.coordsBuffer.length >= this.updates) {
            if (this.coordsBuffer.length > this.updates) {
               console.warn('why coordsBuffer is bigger than required, coordsBuffer.length: ',
                  this.coordsBuffer.length, 'updates', this.updates);
            }
            this.applyBufferedCoords();
            this.lastPoint = coords;
         }
      }
   };

   @action('Set coords') setCoords = (coords: ICoords[]) => {
      if (coords.length) {
         this.coords = coords;
         this.lastPoint = coords[coords.length - 1];
      } else {
         this.coords = [];
         this.lastPoint = DEFAULT_COORDS;
      }
   };

   @action('Set zoom') setZoom = (zoom: number = 9.5) => {
      this.zoom = zoom;
   };

   @action('Set trips') setTrips = (trips: ITrip[]) => {
      this.trips = trips;
      this.initialized = true;
   };

   initHistory = async () => {
      try {
         const {trips} = await api.app.history.read();
         this.setTrips(trips);
      } catch (e) {
         console.error(e);
      }
   };
}
