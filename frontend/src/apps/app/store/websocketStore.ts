import {env} from "@env";

import io from 'socket.io-client';
import {action, observable} from "mobx";
import {ps} from "@ps";


export class WebsocketStore {
   @observable connected = false;

   socket: io.Socket;
   _sid?: string = undefined;

   constructor () {
      this.socket = io(env.WS.FULL_URL);
      this.connect();
   }

   get sid () {
      if (!this._sid) {
         console.error('sid is undefined');
      }
      return this._sid;
   }

   set sid (_sid: string|undefined) {
      this._sid = _sid;
   }

   @action setConnected = (connected: boolean) => {
      this.connected = connected;
   };

   runLiveGlobally = () => {
      this.socket.emit('run live globally');
   };

   runLive = () => {
      this.socket.emit('run live', {sid: this.sid});
   };

   stopLive = () => {
      this.socket.emit('stop live', {sid: this.sid});
   };

   runPlayback = (id: string) => {
      this.socket.emit('run playback', {id, sid: this.sid});
   };

   stopPlayback = (id: string) => {
      this.socket.emit('stop playback', {id, sid: this.sid});
   };

   setSpeed = (speed: number) => {
      this.socket.emit('set speed', {speed, sid: this.sid});
   };

   setUpdates = (updates: number) => {
      this.socket.emit('set updates', {updates, sid: this.sid});
   };

   connect = () => {
      this.socket.on('connect', () => {
         this.setConnected(true);
         this.runLiveGlobally();

         console.log('socket connected !!!');
      });
      this.socket.on('disconnect', () => {
         this.setConnected(false);
         this.sid = undefined;
         if (ps.app.history.playbackMode) {
            ps.app.history.stopPlayback();
         }
         console.log('socket disconnected !!!');
      });

      this.socket.on('event', (data) => {
         console.log('socket got event', data);
      });
      this.socket.on('sid', ({sid}) => {
         if (!this.sid) {
            console.log('set new sid', sid);
            this.sid = sid;
            this.socket.emit('join_to_room', {sid});
            if (ps.app.history.liveMode) {
               ps.app.history.runLive();
            }
         }
      });
      this.socket.on('coords', (data) => {
         ps.app.history.addCoords(data);
      });
      this.socket.on('set coords', (data) => {
         ps.app.history.setCoords(data);
      });
      this.socket.on('end playback', () => {
         ps.app.history.stopPlayback();
      });
   };
}
