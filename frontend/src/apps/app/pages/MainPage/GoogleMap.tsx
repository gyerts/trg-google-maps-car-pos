import React, {useEffect} from 'react';
import {Map, Marker, Polyline, GoogleApiWrapper} from 'google-maps-react';
import {env} from "@env";
import styled from "styled-components";
import {observer} from "mobx-react";
import {ps} from "@ps";
import {ICoords} from "@app/store/appStore";

const Container = styled.div`
   width: 100vw;
   height: 100vh;
   position: fixed;
   top: 0;
   left: 0;
   
   > div > div > div:nth-child(2) {
      display: none;
   }
   
   .gm-control-active.gm-fullscreen-control {
      display: none;      
   }
`;

const Coords = observer(({setCoords, setLastPoint, setZoom}) => {
   useEffect(function () {
      setCoords(ps.app.history.coords);
   }, [ps.app.history.coords]);

   useEffect(function () {
      setCoords(ps.app.history.coords);
      setLastPoint({...ps.app.history.lastPoint});
   }, [ps.app.history.lastPoint]);

   useEffect(function () {
      setZoom(ps.app.history.zoom);
   }, [ps.app.history.zoom]);

   return null;
});

type IState = {
   coords: ICoords[]
   lastPoint: ICoords
   zoom: number
};


class GoogleMapContainer extends React.Component<{google}, IState> {
   state: IState = {
      coords: ps.app.history.coords,
      lastPoint: ps.app.history.lastPoint,
      zoom: ps.app.history.zoom,
   };

   componentDidUpdate(prevProps: Readonly<{ google }>, prevState: Readonly<IState>, snapshot?: any): void {
      console.log(prevProps);
   }

   setCoords = (coords: ICoords[]) => {
      this.setState({
         coords: [
            ...coords,
         ],
      });
   };

   setLastPoint = (lastPoint: ICoords) => {
      this.setState({
         lastPoint,
      });
   };

   setZoom = (zoom: number) => {
      this.setState({
         zoom,
      });
   };

   render () {
      return (
         <Container>
            <Coords setCoords={this.setCoords} setLastPoint={this.setLastPoint} setZoom={this.setZoom} />
            <Map
               google={this.props.google}
               style={{
                  width: '100%',
                  height: '100%',
               }}
               center={this.state.lastPoint}
               // @ts-ignore
               zoom={this.state.zoom}
            >
               <Marker
                  //@ts-ignore
                  icon={{
                     url: '/img/car-point.png',
                     scaledSize: new window['google'].maps.Size(50, 50)
                  }}
                  position={this.state.lastPoint}
               />
               <Polyline path={this.state.coords} />
            </Map>
         </Container>
      );
   }
}

export const GoogleMap = GoogleApiWrapper({
   apiKey: env.GOOGLE_KEY
})(GoogleMapContainer);
