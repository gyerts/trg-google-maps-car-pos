import React, {useCallback, useEffect} from 'react';
import {observer} from "mobx-react";
import styled from "styled-components";
import {ps} from "@ps";
import {Button, InputNumber} from "antd";
import {TripSelector} from "@app/pages/MainPage/TripSelector";
import {GoogleMap} from "@app/pages/MainPage/GoogleMap";

const Container = styled.div`
   display: flex;
   width: 100%;
   height: 100%;
   flex-direction: column;
`;

const ContainerInfo = styled.div`
   display: flex;
   padding: 1rem;
   width: 100%;
   height: 100%;
   flex-direction: column;
   z-index: 5;
   background-color: #ffffffc7;
   
   div.cell {
      display: flex;
      width: 100%;
      justify-content: center;
      align-items: center;
   }
`;

const Connected = styled.div`
   width: 10px;
   height: 10px;
   border-radius: 10px;
   display: inline-block;
   background-color: ${p => p.connected ? 'green' : 'red'}
`;

const Space = styled.div`
   display: inline-block;
   width: 1rem;
`;

const SpaceShort = styled.div`
   display: inline-block;
   width: 5px;
`;

const InputContainer = styled.div`
   display: inline-block;
   position: relative;
   min-width: 2rem;
   
   span.label {
      position: absolute;
      top: -10px;
      font-size: 8px;
      line-height: 10px;
   }
`;

export default observer(() => {
   useEffect(function () {
      ps.app.history.runLive();
   }, []);

   const handleSpeedChanged = useCallback(function (speed: string|number|undefined) {
      ps.app.history.setSpeed(parseInt(`${speed}` || '1'))
   }, []);

   const handleUpdatesChanged = useCallback(function (updates: string|number|undefined) {
      ps.app.history.setUpdates(parseInt(`${updates}` || '1'))
   }, []);

   return (
      <Container>
         <GoogleMap />

         <ContainerInfo>
            <div className='cell'>
               <span>ws {ps.app.ws.connected ? 'connected' : 'disconnected'}</span>
               <Space />
               <Connected connected={ps.app.ws.connected} />
               <Space />
               <InputContainer>
                  <span className='label'>points</span>
                  <span>{ps.app.history.coords.length}</span>
               </InputContainer>

               <Space />
               <InputContainer>
                  <span className='label'>speed (only playback)</span>
                  <InputNumber
                     onChange={handleSpeedChanged}
                     defaultValue={ps.app.history.speed}
                     size='small'
                     step={0.1}
                     min={1}
                     disabled={ps.app.history.liveMode}
                  />
               </InputContainer>
               <Space />
               <InputContainer>
                  <span className='label'>updates</span>
                  <InputNumber
                     onChange={handleUpdatesChanged}
                     defaultValue={ps.app.history.updates}
                     size='small'
                     min={1}
                  />
               </InputContainer>
               <Space />
               <TripSelector width={'50%'} onChange={ps.app.history.setSelectedTripId} selectedId={ps.app.history.selectedTripId} />
               <Space />
               {ps.app.history.selectedTripId && !ps.app.history.playbackMode && (
                  <Button size='small' onClick={ps.app.history.runPlayback}>
                     Run playback
                  </Button>
               )}
               {ps.app.history.selectedTripId && ps.app.history.playbackMode && (
                  <Button size='small' type={"dashed"} onClick={ps.app.history.stopPlayback}>
                     Stop playback
                  </Button>
               )}
               <Space />
               {!ps.app.history.playbackMode && !ps.app.history.liveMode && (
                  <Button size='small' type={"primary"} onClick={ps.app.history.runLive}>
                     <Connected connected={true} /><SpaceShort />Live
                  </Button>
               )}
            </div>
         </ContainerInfo>
      </Container>
   );
});
