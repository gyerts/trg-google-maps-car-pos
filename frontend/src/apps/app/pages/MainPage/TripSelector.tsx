import React, {useCallback} from 'react';
import {observer} from "mobx-react";
import {Select} from "antd";
import {ps} from "@ps";

export const TripSelector = observer(({width, selectedId, onChange}) => {
   const handleChange = useCallback(function (id: string) {
      onChange(id);
   }, []);

   return (
      <Select
         style={{ width }}
         onChange={handleChange}
         value={selectedId}
         size='small'
         placeholder='select trip in history'
      >
         {ps.app.history.trips.map(trip => (
            <Select.Option key={trip.id} value={trip.id}>{trip.name}</Select.Option>
         ))}
      </Select>
   );
});
