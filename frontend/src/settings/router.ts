import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
const createHistory = require("history").createHashHistory;

export const routingStore = new RouterStore();
export const history = syncHistoryWithStore(createHistory(), routingStore);
