import {useCallback, useState} from 'react';
import {
   Form,
} from 'antd';
import {FormInstance} from "antd/lib/form";
import {l} from "@utils/logger";

// ---------------------------------------------------
// types
// ---------------------------------------------------
type IProps<T> = {
   onSubmit: (values: T) => Promise<void>
   postSubmitAction?: (values: T) => void
};

// ---------------------------------------------------
// class
// ---------------------------------------------------
export const useOnFinish = <T>(props: IProps<T>): {
   loading: boolean, setLoading: (loading: boolean) => void,
   errors: boolean, setErrors: (errors: {}) => void,
   onFinish: any, onFinishFailed: any,
   form: FormInstance,
} => {
   const [loading, setLoading] = useState(false);
   const [errors, setErrors] = useState<any>({});
   const [form] = Form.useForm();

   const onFinishFailed = () => {
   };

   const onFinish = useCallback(async (values: T) => {
      setErrors({});
      setLoading(true);

      try {
         await props.onSubmit(values);
         props.postSubmitAction && props.postSubmitAction(values);
      } catch (errors) {
         if (errors && errors.validation) {
            const formErrors = {};
            Object.keys(errors.validation).map(field => {
               formErrors[field] = {
                  hasFeedback: true,
                  validateStatus: "error",
                  help: errors.validation[field]
               }
            });
            setErrors(formErrors);
         } else {
            l.error(errors);
         }
      }

      setLoading(false);
   }, []);

   return {
      loading, setLoading,
      errors, setErrors,
      onFinish, onFinishFailed,
      form,
   };
};
