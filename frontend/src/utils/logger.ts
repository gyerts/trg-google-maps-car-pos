const getConsoleLogger = () => {
   return {
      debug: console.debug,
      log: console.log,
      info: console.info,
      warn: console.warn,
      table: console.table,
      error: process.env.NODE_ENV === 'production' ? console.error : (e: any) => {
         console.error(e);
         // alert(e);
         // debugger;
      }
   };
};

export let l = getConsoleLogger();
