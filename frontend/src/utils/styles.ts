export function get_computed_style(id, name){
   const element = document.getElementById(id);
   if (element) {
      return window.getComputedStyle(element, null).getPropertyValue(name);
   }
}
