import qs from 'qs';

export const serialize = (objects: (Object|undefined)[], prefix: string) => {
   const opts = {encodeValuesOnly: true, indices: false, arrayFormat: 'comma'};
   let allObjectsTogether = {};

   objects.forEach(o => {
      o && (allObjectsTogether = {...allObjectsTogether, ...o});
   });

   return `${prefix}?${qs.stringify(allObjectsTogether, opts)}`;
};
