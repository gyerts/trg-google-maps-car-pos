import {l} from "@utils/logger";
import {validateResponse} from "../jsonschema/validate";

export type IResponseError = {
   validation?: {
      [key: string]: string // поле формы к примеру
   }
}

export const fetchWrapper = async<T extends { errors?: IResponseError }>(
   url: RequestInfo,
   init: RequestInit,
   schema?: any,
): Promise<T> => {
   let response = await fetch(url, init);

   const data: T = await response.json();

   if (!data.errors) {
      const isResponseValid = schema ? validateResponse(data, schema) : true;
      if (!isResponseValid) {
         throw new Error(`${url}: invalid validation of response`);
      }
      return data;
   }

   l.error(data.errors);
   return data;
};
