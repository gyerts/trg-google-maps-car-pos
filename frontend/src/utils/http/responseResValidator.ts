import { Schema, Validator } from "jsonschema";
import {l} from "@utils/logger";

export const validator = new Validator();
l.debug(validator);

export function isValid<T>(jsonObj: T, schema: Schema){
   const res = validator.validate(jsonObj, schema);
   if (res.errors.length !== 0) {
      l.error(`Validation of schema failed: ${schema.id}`);
      l.debug(jsonObj);
      l.error(res.errors.map(er => er.toString()));
      l.table({ income: res.errors });
   }
   return res.errors.length === 0;
}

