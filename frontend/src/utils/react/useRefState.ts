import {useCallback, useRef, useState} from "react";

export function useRefState<T>(initialValue: T): [
   T, {current: T}, (value: T) => void
   ] {
   const [value, _setValue] = useState<T>(initialValue);

   const setValue = useCallback((value: T) => {
      valueRef.current = value;
      _setValue(value)
   }, []);

   const valueRef = useRef(value);
   return [value, valueRef, setValue];
}
