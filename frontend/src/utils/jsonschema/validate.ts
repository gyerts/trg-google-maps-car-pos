import {validator} from "../http/responseResValidator";
import {l} from "@utils/logger";

export const validateResponse = (data: any, schema: any) => {
   l.debug(data, schema);

   const validationResult = validator.validate(data, schema);

   if (validationResult.errors.length) {
      l.error(validationResult);
      l.table({
         schema: validationResult.errors[0].schema,
         message: validationResult.errors[0].message,
      });
      l.error('invalid response (jsonshema)');
   }
   return true;
};
