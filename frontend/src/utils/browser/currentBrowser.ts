export type IBrowserType = 'safari' | 'edge' | 'opera' | undefined;

export const getCurrentBrowser = (): IBrowserType => {
   if (/^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
      return 'safari'
   } else if (/Edge/.test(navigator.userAgent)) {
      return 'edge'
      // @ts-ignore
   } else if (!!window.opera || navigator.userAgent.indexOf('OPR/') !== -1) {
      return 'opera'
   }

   return;
};