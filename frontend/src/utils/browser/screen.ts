export function isLandscape () { return window.innerHeight < window.innerWidth; }
export function isMobile () { return window.navigator.userAgent.toLowerCase().includes(' mobile ') }
export function isX96mini () { return window.navigator.userAgent.toLowerCase().includes(' x96mini ') }
