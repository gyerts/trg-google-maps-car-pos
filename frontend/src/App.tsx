import React from 'react';
import {Route} from "react-router-dom";

import './App.scss';

import {Routes} from "@components/RoutesLoadable";
import {appLoadableRouts, appRoutes} from "@app/routes";
import {TopBarLoader} from "@components/TopBarLoader";

export default function App () {
   return (
      <div>
         <div style={{position: 'fixed', top: 0, left: 0, width: '100%', zIndex: 999}}>
            <TopBarLoader />
         </div>
         <Route key={appRoutes.main.url} path={appRoutes.main.url}>
            <Routes loadableRouts={appLoadableRouts} />
         </Route>
      </div>
   );
}
