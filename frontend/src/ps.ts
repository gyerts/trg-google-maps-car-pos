import { history } from './settings/router';
import {appRoutes} from "@app/routes";
import {Store} from "@app/ps";

const PS_ROUTER = {
   goBack: history.goBack,
   goForward: history.goForward,
   push: (url: string) => {
      history.push(url)
   },
   replace: history.replace,
   path: {
      save: (url: string) => {
         localStorage.setItem('redirect-url', url);
      },
      goToSavedUrl: () => {
         const restoreUrl = localStorage.getItem('redirect-url') || appRoutes.main.url;
         localStorage.removeItem('redirect-url');
         ps.router.replace(restoreUrl);
      }
   }
};

export const ps: {
   router: typeof PS_ROUTER,
   app: Store,
} = window['ps'] = {
   router: PS_ROUTER,
   app: new Store(),
};
