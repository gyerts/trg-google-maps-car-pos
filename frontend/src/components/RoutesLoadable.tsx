import React, {useEffect} from "react";
import Loadable from 'react-loadable';
import {useLocation} from 'react-router-dom';
import { LoadableComponent } from 'react-loadable';
import { Route, Switch } from 'react-router-dom';


export const loadable = loader =>
   Loadable({
      loader,
      delay: false,
      loading: () => null,
   });

type IProps = {
   loadableRouts: {
      [key: string]: {
         component: React.ComponentType<any> & LoadableComponent,
         exact?: boolean
      }
   }
}

export function Routes (props: IProps) {
   const location = useLocation();

   useEffect(function () {
      setTimeout(
         () =>
            Object.keys(props.loadableRouts).forEach(path => props.loadableRouts[path].component.preload()),
         5000, // остальные компоненты что не загружены загрузим через 5 секунд
      );
   }, [props.loadableRouts]);

   return (
      <Switch location={location}>
         {Object.keys(props.loadableRouts).map(path => {
            const route = props.loadableRouts[path];
            return <Route key={path} path={path} {...route} exact={Boolean(route.exact)} />;
         })}
      </Switch>
   )
}
