import React, {useEffect, useState} from "react";
import LoadingBar from 'react-top-loading-bar';
import { observable, action } from "mobx";
import {observer} from "mobx-react";


class TopBarLoaderStore {
   @observable loading = false;
   @action('Run loading line on top') setLoading = (loading: boolean) => {
      this.loading = loading;
   }
}

export const topBarLoaderStore = new TopBarLoaderStore();

export const TopBarLoader: React.FC = observer(() => {
   const [ref, setRef] = useState<any>(undefined);

   useEffect(() => {
      if (ref) {
         if (topBarLoaderStore.loading) {
            ref['continuousStart']();
         } else {
            ref['complete']();
         }
      }
      // topBarLoaderStore.loading это не просто глобальная переменная, это mobx observable, но lint про это
      // ничего не знает конечно, и он думает что я никогда не получу обновленное состояние тут
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [topBarLoaderStore.loading, ref]);

   return (
      <LoadingBar
         height={3}
         color='#43B0F1'
         onRef={ref => setRef(ref)}
      />
   );
});
