import {TwitterPicker as OriginTwitterPicker} from "react-color";
import React, {useCallback} from "react";

type IProps = {
   defaultColor?: string
   onChange: (hex: string) => void
};

export const TwitterPicker = (props: IProps) => {
   const onChange = useCallback(function ({hex}) {
      props.onChange(hex);
   }, []);

   return (
      <OriginTwitterPicker
         color={props.defaultColor}
         onChange={onChange}
      />
   );
};

