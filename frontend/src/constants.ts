export const placeholder = '';

export const LS_SELECTED_COMPANY_ID = 'app.companies.selectedCompany';
export const SELECTED_COMPANY_ID = localStorage.getItem(LS_SELECTED_COMPANY_ID);
