import {validator} from "@utils/http/responseResValidator";

export const tripSchema = {
   id: '/tripSchema',
   type: 'object',
   properties: {
      id: { type: 'string' },
      name: { type: 'string' },
   },
   required: [
      'id',
      'name',
   ],
};

validator.addSchema(tripSchema, tripSchema.id);
