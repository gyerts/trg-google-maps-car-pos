import {ICoords} from "@app/store/appStore";

export type ITrip = {
   id: string
   name: string
   coordinates: ICoords[]
}
