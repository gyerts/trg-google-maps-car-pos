import {IResponseError} from "@utils/http/fetchWrapper";

export const defaultResponseSchemaFields = {
   statusCode: {
      type: 'integer',
      enum: [
         200, // success
         204, // success, no data
         400, // bad data in request
         401, // not allowed to see resource
         404, // no resource found
      ]
   },
   errors: {
      type: 'object',
      properties: {
         validation: { type: 'object', properties: {} },
      }
   },
};


export type IDefaultResponseSchema<T extends object> = {
   statusCode: number
   data: T
   errors?: IResponseError
}

export const defaultRequiredResponseSchemaFields = [
   'data',
   'statusCode',
];
