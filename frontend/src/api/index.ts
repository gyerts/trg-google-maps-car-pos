import {appRequests} from "@app/api";

export const api = {
   app: appRequests
};

window['api'] = api;
