import {env} from "@env";

export const BASE_URL = (url: string) => env.BACKEND.FULL_URL + '/api' + url;

export const AUTH_HEADER = () => {
   return {
      Authentication: `Bearer xxx`
   }
};

export const CONTENT_TYPE_HEADER = () => ({
   'Content-Type': 'application/json;charset=utf-8',
});
