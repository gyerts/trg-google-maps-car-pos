import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { Router } from 'react-router';

import * as serviceWorker from './serviceWorker';
import '@utils/logger';

import App from './App';
import {routingStore, history} from "./settings/router";
import {l} from "@utils/logger";
import {env} from "@env";

l.log('===================================');
l.log(JSON.stringify(env, null, 2));
l.log('===================================');

const stores = {
   // Key can be whatever you want
   routing: routingStore,
   // ...other stores
};

function renderApplication () {
   const app = (
      <Provider {...stores}>
         <Router history={history}>
            <App />
         </Router>
      </Provider>
   );

   ReactDOM.render(app, document.getElementById('root'));
}

renderApplication();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
