FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

WORKDIR .
RUN apk add --no-cache gcc make musl-dev linux-headers bash postgresql-dev \
    && apk update \
    && apk add jpeg-dev \
               zlib-dev \
               freetype-dev \
               lcms2-dev \
               openjpeg-dev \
               tiff-dev \
               tk-dev \
               tcl-dev \
               harfbuzz-dev \
               fribidi-dev \
               nodejs \
               yarn \
               nodejs-npm \
               npm
#    && apk add jpeg-dev zlib-dev libjpeg \
#    && pip install Pillow \
#    && apk del build-deps

COPY . /flask_application
COPY ./trips.geo.json /
COPY ./live.geo.json /

RUN echo '' > /flask_application/frontend/xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

RUN pip3 install gunicorn
RUN pip3 install gevent
RUN pip3 install -r flask_application/backend/requirements.txt

#WORKDIR /flask_application/frontend
#RUN ls
#RUN yarn install
#RUN yarn build
#RUN ls
